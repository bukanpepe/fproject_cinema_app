from flask import Flask, jsonify, request, render_template, url_for, redirect
from sqlalchemy import create_engine
from sqlalchemy.sql import text
import datetime
import decimal

app = Flask(__name__)

conn_str = 'postgresql://postgres:Putranurelva2804?@localhost:5000/cinema_DB'
engine = create_engine(conn_str, echo=False)

@app.route('/')
def defaultUrl():
    return redirect(url_for('index'))

@app.route('/index', methods=['GET'])
def index():
    try:
        with engine.connect() as connection:
            get_movie = text("SELECT title, description, directed_by FROM movie")
            movie_res = connection.execute(get_movie)
            return render_template('index.html', movies=movie_res)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/index/<title>', methods=['GET'])
def details(title):
    try:
        with engine.connect() as connection:
            get_detail = text(f"SELECT * FROM movie WHERE title = '{title}'")
            detail_res = connection.execute(get_detail)
            for i in detail_res:
                title = i[1]
                desc = i[2]
                directed = i[3]
                release = i[4]
                duration = i[5]
            return render_template('details.html', title=title, description=desc, directed_by=directed, release_year=release, duration=duration)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/index/', methods=['GET'])
def search_movie():
    val = request.args.get("search")
    try:
        with engine.connect() as connection:
            if val.isdigit():
                get_detail = text(f"SELECT title, description, directed_by, release_year, duration_mins FROM movie WHERE (title LIKE '%{val}%' OR description LIKE '%{val}%' OR directed_by LIKE '%{val}%' OR release_year = '{val}')")
            else:
                get_detail = text(f"SELECT title, description, directed_by, release_year, duration_mins FROM movie WHERE (title LIKE '%{val}%' OR description LIKE '%{val}%' OR directed_by LIKE '%{val}%')")

            detail_res = connection.execute(get_detail)
            return render_template('search.html', movies=detail_res)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/index/movies/upcoming')
def indexUpcoming():
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT title, description, directed_by, release_year, duration_mins FROM movie WHERE movie_id NOT IN (Select movie_id from schedule group by 1)")
            movies = connection.execute(qry)
            return render_template('index_mov_rep.html', movies=movies)
    except Exception as e:
        return jsonify(str(e))

@app.route('/index/movies/mostwatched')
def indexMost_watched():
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT title, description, directed_by, release_year, duration_mins, SUM(ticket_qty) as total FROM ticket JOIN schedule USING(schedule_id) JOIN movie USING(movie_id) GROUP BY 1,2,3,4,5 ORDER BY 6 DESC LIMIT 5")
            movies = connection.execute(qry)
            return render_template('index_mov_rep.html',  topmovies=movies)
    except Exception as e:
        return jsonify(str(e))

@app.route('/signup')
def signUp():
    return render_template('add_user.html')

@app.route('/signup/newuser', methods = ['POST'])
def regUser():
    form_data = request.form
    result = [value for key, value in form_data.items()]
    try :
        with engine.connect() as connection:
            try:
                check_email = text(f"SELECT email_address FROM public.user WHERE email_address = '{result[1]}'")
                run_verify = connection.execute(check_email)
                for h in run_verify:
                    verification = str(h[0])

                if verification == result[1]:
                    return render_template('add_user.html', alert='That email address is already being used.\nPlease use another email address')
            except Exception:
                qry = text(f"INSERT INTO public.user (username, email_address, user_address) VALUES ('{result[0]}', '{result[1]}', '{result[2]}')")
                commit = connection.execute(qry)
                user = text(f"SELECT * FROM public.user WHERE username='{result[0]}' AND email_address='{result[1]}'")
                get_user = connection.execute(user)
                for i in get_user:
                    user_id = i[0]

                if result[0] == 'Admin' and result[1] == 'admin@cinemadmin.com':
                    return redirect(url_for('adminDash'))
                else:
                    return redirect(url_for('userIndex', user_id=user_id))

    except Exception as e:
        print(str(e))
        return redirect(url_for('signUp'))

@app.route('/login')
def logIn():
    return render_template('login.html')

@app.route('/login/user', methods=['POST'])
def logged():
    user = []
    try:
        form_data = request.form
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            qry = text(f"SELECT * FROM public.user WHERE username = '{result[0]}' AND email_address = '{result[1]}'")
            get = connection.execute(qry)
            for i in get:
                user_id = i[0]
                username = i[1]
                email = i[2]
            if username == 'Admin' and email == 'admin@admin.com':
                return redirect(url_for('adminDash'))
            else:
                return redirect(url_for('userIndex', user_id=user_id))
    except Exception as e:
        print(str(e))
        return redirect(url_for('logIn'))

@app.route('/user/<user_id>/schedule', methods=['GET'])
def userIndex(user_id):
    play_times = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT user_id, username, email_address, balance FROM public.user WHERE user_id='{user_id}'")
            result = connection.execute(qry)
            for i in result:
                get_id = i[0]
                username = i[1]
                balance = i[3]
            schedule = text("SELECT play_date, day_id, day_name, movie_id, title, studio_id, price FROM public.schedule JOIN day_schedule using(day_id) JOIN movie using(movie_id) GROUP BY 1,2,3,4,5,6,7 ORDER BY day_id, studio_id, movie_id")
            get_sched = connection.execute(schedule)
            return render_template('user_dash.html', username=username, user_id=get_id, balance=balance, movies = get_sched)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/index/', methods=['GET'])
def user_movie_search(user_id):
    val = request.args.get("search")
    try:
        with engine.connect() as connection:
            if val.isdigit():
                get_detail = text(f"SELECT studio_id, title, description, directed_by, release_year, duration_mins, play_date FROM movie JOIN schedule USING (movie_id) WHERE (title LIKE '%{val}%' OR description LIKE '%{val}%' OR directed_by LIKE '%{val}%' OR release_year = '{val}') GROUP BY 1,2,3,4,5,6,7 ORDER BY play_date, studio_id")
            else:
                get_detail = text(f"SELECT studio_id, title, description, directed_by, release_year, duration_mins, play_date FROM movie JOIN schedule USING (movie_id) WHERE (title LIKE '%{val}%' OR description LIKE '%{val}%' OR directed_by LIKE '%{val}%') GROUP BY 1,2,3,4,5,6,7 ORDER BY play_date, studio_id")
            detail_res = connection.execute(get_detail)
            return render_template('user_movie_search.html', user_id=user_id, movies=detail_res)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/user/<user_id>/profile')
def userProfile(user_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT * FROM public.user WHERE user_id = {user_id}")
            user = connection.execute(qry)
        return render_template('user_profile.html', user_id=user_id, profile=user)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/profile/edit', methods=['POST'])
def editProfile(user_id):
    form_data = request.form
    result = [value for key, value in form_data.items() if value]
    print(result)
    try:
        with engine.connect() as connection:
            update = text(f"UPDATE public.user SET username='{result[0]}', user_address='{result[1]}' WHERE user_id={user_id}")
            commitUpdate = connection.execute(update)
            return redirect(url_for('userProfile', user_id=user_id))
    except Exception as e:
        return jsonify(str(e))
  
@app.route('/user/<user_id>/upcoming')
def upcoming(user_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT title, description, directed_by, release_year, duration_mins FROM movie WHERE movie_id NOT IN (Select movie_id from schedule group by 1)")
            movies = connection.execute(qry)
            return render_template('user_mov_rep.html', user_id=user_id, movies=movies)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/mostwatched')
def most_watched(user_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT title, description, directed_by, release_year, duration_mins, SUM(ticket_qty) as total FROM ticket JOIN schedule USING(schedule_id) JOIN movie USING(movie_id) GROUP BY 1,2,3,4,5 ORDER BY 6 DESC LIMIT 5")
            movies = connection.execute(qry)
            return render_template('user_mov_rep.html', user_id=user_id, topmovies=movies)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/topup')
def topUp(user_id):
    return render_template('topUp.html', user_id=user_id)

@app.route('/user/<user_id>/topup/add', methods=['POST'])
def topUpAdd(user_id):
    form_data = request.form
    amount = [value for key, value in form_data.items()]
    try:
        with engine.connect() as connection:
            qry = text(f"UPDATE public.user SET balance='{amount[0]}' WHERE user_id='{user_id}'")
            commit = connection.execute(qry)
            updateTrx = text(f"INSERT INTO public.transaction (user_id, top_up) VALUES ({user_id},{amount[0]})")
            updateTrxCommit = connection.execute(updateTrx)
        return redirect(url_for('userIndex', user_id=user_id))

    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/buy', methods=['GET','POST'])
def moviePick(user_id):
    title = request.args.get('title')
    play_date = request.args.get('date')
    times = []
    try:
        with engine.connect() as connection:
            movie = text(f"SELECT movie_id, title, description, directed_by, release_year, duration_mins, string_agg(cat_name,', ') as category from movie join movie_cat using(movie_id) join category using(category_id) WHERE title='{title}' GROUP BY 1")
            get_movie = connection.execute(movie)
            for i in get_movie:
                movie_id = i[0]
                title = i[1]
                desc = i[2]
                directed = i[3]
                release = i[4]
                duration = i[5]
                cat = i[6] 
            schedule = text(f"SELECT start_time, play_date FROM schedule WHERE play_date='{play_date}' AND movie_id='{movie_id}'")
            get_sched = connection.execute(schedule)
            for j in get_sched:
                times.append(j[0].strftime("%H:%M"))
        return render_template('buy_ticket.html',title=title, play_date=play_date, description=desc, directed_by=directed, release_year=release, duration=duration, category=cat, schedule=times, user_id=user_id)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/buy/ticket', methods=['POST', 'GET'])
def confirmTix(user_id):
    title = request.args.get('title')
    play_date = request.args.get('date')
    time = request.args.get('time')
    try:
        with engine.connect() as connection:
            movie = text(f"SELECT movie_id, title, description, directed_by, release_year, duration_mins, string_agg(cat_name,', ') as category from movie join movie_cat using(movie_id) join category using(category_id) WHERE title='{title}' GROUP BY 1")
            get_movie = connection.execute(movie)
            for i in get_movie:
                movie_id = i[0]
                title = i[1]
                desc = i[2]
                directed = i[3]
                release = i[4]
                duration = i[5]
                cat = i[6] 

            priceQry = text(f"SELECT price FROM schedule JOIN day_schedule USING(day_id) WHERE play_date='{play_date}' AND start_time='{time}'")
            get_price = connection.execute(priceQry)
            for j in get_price:
                price = j[0]
        return render_template('confirmTix.html',title=title, play_date=play_date, description=desc, directed_by=directed, release_year=release, duration=duration, category=cat, price=price, time=time, user_id=user_id)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/buy/ticket/confirm', methods=['POST', 'GET'])
def purchaseTix(user_id):
    form_data = request.form
    data = [value for key, value in form_data.items()]
    qty = int(data[0])
    title = request.args.get('title')
    play_date = request.args.get('date')
    time = request.args.get('time')
    try:
        with engine.connect() as connection:
            check_bal = text(f"SELECT balance FROM public.user WHERE user_id='{user_id}'")
            run_check_bal = connection.execute(check_bal)
            for i in run_check_bal:
                if i[0] == None:
                    return render_template('error.html', user_id=user_id, alert='Jumlah saldo tidak mencukupi')
                else:
                    curBal = int(i[0])
            
            movieQry = text(f"SELECT movie_id FROM movie WHERE title='{title}'")
            get_movieid = connection.execute(movieQry)
            for j in get_movieid:
                movie_id = j[0]

            priceQry = text(f"SELECT schedule_id, price, capacity FROM schedule JOIN day_schedule USING(day_id) JOIN studio USING(studio_id) WHERE play_date='{play_date}' AND start_time='{time}' AND movie_id='{movie_id}'")
            get_price = connection.execute(priceQry)
            for k in get_price:
                schedule_id = k[0]
                price = int(k[1])
                capacity = k[2]

            check_seat = text(f"SELECT SUM(ticket_qty) FROM ticket WHERE schedule_id = {schedule_id}")
            run_check_seat = connection.execute(check_seat)
            for l in run_check_seat:
                seatTaken = j[0]

            totPrice = qty * price
            finalBal = curBal - totPrice

            if qty == 0 or qty == None:
                return render_template('error.html', user_id=user_id, alert='Jumlah ticket harus diisi')
            elif totPrice > curBal:
                return render_template('error.html', user_id=user_id, alert='Jumlah saldo tidak mencukupi')
            elif seatTaken > capacity:
                return render_template('error.html', user_id=user_id, alert='Mohon maaf, tempat duduk sudah penuh')
            else:
                updateTix = text(f"INSERT INTO ticket (schedule_id, user_id, ticket_qty) VALUES ({schedule_id}, {user_id}, {qty})")
                updateTixCommit = connection.execute(updateTix)
                updateTrx = text(f"INSERT INTO public.transaction (user_id, purchase) VALUES ({user_id},{totPrice})") 
                updateTrxCommit = connection.execute(updateTrx)
                updateBal = text(f"UPDATE public.user SET balance={finalBal} WHERE user_id={user_id}")
                updateBalCommit = connection.execute(updateBal)
                return redirect(url_for('userIndex', user_id=user_id))

    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/trx/history')
def trxHistory(user_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT trx_id, top_up, purchase, trx_stamp FROM public.transaction WHERE user_id={user_id}")
            history = connection.execute(qry)
            return render_template('user_trxs.html', user_id=user_id, history=history)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/tix/history')
def tixHistory(user_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT ticket_id, schedule_id, ticket_qty FROM public.ticket WHERE user_id={user_id}")
            history = connection.execute(qry)
            return render_template('user_tix.html', user_id=user_id, history=history)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/tix/<ticket_id>')
def tixDetail(user_id, ticket_id):
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT ticket_id, title, play_date, description, directed_by, start_time, ticket_qty FROM public.ticket JOIN schedule USING(schedule_id) JOIN movie USING(movie_id) WHERE user_id={user_id} AND ticket_id={ticket_id}")
            detail = connection.execute(qry)
            return render_template('user_tix_detail.html', user_id=user_id, detail=detail)
    except Exception as e:
        return jsonify(str(e))


@app.route('/admin')
def adminDash():
    return render_template('admin_dash.html')

@app.route('/admin/report/user', methods=['GET'])
def allUser():
    try :
        with engine.connect() as connection:
                qry = text("SELECT * FROM public.user")
                result = connection.execute(qry)
                return render_template('admin_user.html', list=result)                
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/admin/report/user/', methods=['GET'])
def sortUser():
    get_param = request.args.get("sort_by")
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT * FROM public.user ORDER BY {get_param}")
            result = connection.execute(qry)
            return render_template('admin_user.html', list=result)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/admin/report/tixpurchase', methods=['GET'])
def allPurchase():
    try :
        with engine.connect() as connection:
                qry = text("SELECT play_date, start_time, movie_id, title, user_id, trx_id, ticket_qty, purchase, trx_stamp FROM ticket JOIN public.transaction USING(user_id) JOIN schedule USING(schedule_id)JOIN movie USING(movie_id) WHERE purchase > 0")
                result = connection.execute(qry)
                return render_template('admin_tixpurchase.html', list=result)                
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/admin/report/tixpurchase/', methods=['GET'])
def sortPurchase():
    get_param = request.args.get("sort_by")
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT play_date, start_time, movie_id, title, user_id, trx_id, ticket_qty, purchase, trx_stamp FROM ticket JOIN public.transaction USING(user_id) JOIN schedule USING(schedule_id) JOIN movie USING(movie_id) WHERE purchase > 0ORDER BY {get_param}")
            result = connection.execute(qry)
            return render_template('admin_tixpurchase.html', list=result)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/admin/report/sales', methods=['GET'])
def allMovie():
    try:
        with engine.connect() as connection:
            get_report = text("SELECT *, (SELECT SUM(ticket_qty) as ticket_sold FROM ticket as tix JOIN schedule as sched USING(schedule_id) JOIN day_schedule USING(day_id) JOIN studio USING(studio_id) JOIN movie USING(movie_id) WHERE sched.movie_id = mov.movie_id), (SELECT SUM(ticket_qty*price) as total_earning FROM ticket as tix JOIN schedule as sched USING(schedule_id) JOIN day_schedule USING(day_id) JOIN studio USING(studio_id) JOIN movie USING(movie_id) WHERE sched.movie_id = mov.movie_id) FROM movie as mov ORDER BY movie_id;")
            report = connection.execute(get_report)
            return render_template('admin_salesrep.html', list=report)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies')
def getMovies():
    try:
        with engine.connect() as connection:
            get_movies = text("SELECT * FROM movie ORDER BY movie_id;")
            movies = connection.execute(get_movies)
            return render_template('admin_editmovie.html', list=movies)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies/add', methods=['POST'])
def addMovie():
    try:
        form_data = request.form
        result = [value for key, value in form_data.items()]    
        with engine.connect() as connection:
            add = text(f"INSERT INTO movie (title, description, directed_by, release_year, duration_mins) VALUES('{result[0]}','{result[1]}','{result[2]}',{result[3]},{result[4]})")
            commitAdd = connection.execute(add)
        return redirect(url_for('getMovies'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies/update', methods=['POST'])
def editMovie():
    try:
        form_data = request.form
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            edit = text(f"UPDATE movie SET title='{result[1]}', description='{result[2]}', directed_by='{result[3]}', release_year='{result[4]}', duration_mins='{result[5]}' WHERE movie_id={result[0]}")
            commitEdit = connection.execute(edit)
        return redirect(url_for('getMovies'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies/remove', methods=['DELETE'])
def removeMovie():
    try:
        form_data = request.form
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            remove = text(f"DELETE FROM movie WHERE movie_id={result[0]}")
            commitRemove = connection.execute(remove)
        return redirect(url_for('getMovies'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule')
def getSchedule():
    try:
        with engine.connect() as connection:
            get_movies = text("SELECT movie_id, title FROM movie")
            legend = connection.execute(get_movies)

            studio1_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 1")
            studio1 = connection.execute(studio1_movies)

            studio2_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 2")
            studio2 = connection.execute(studio2_movies)

            studio3_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 3")
            studio3 = connection.execute(studio3_movies)

            studio4_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 4")
            studio4 = connection.execute(studio4_movies)

        return render_template('admin_editschedule.html', legend=legend, studio1=studio1, studio2=studio2, studio3=studio3, studio4=studio4)
    
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule/add', methods=['POST'])
def addSchedule():
    try:
        form_data = request.form
        result = [value for key, value in form_data.items() if value]
        times = result[4:]
        print(times, result)
        with engine.connect() as connection:
            for i in times:
                add = text(f"INSERT INTO schedule (day_id, play_date, movie_id, studio_id, start_time) VALUES ({result[1]},{result[2]},{result[3]},{result[0]},{i})")
                commitAdd = connection.execute(add)
        return redirect(url_for('getSchedule'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule/update', methods=['POST'])
def editSchedule():
    try:
        form_data = request.form
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            edit = text(f"UPDATE schedule SET play_date='{result[1]}', day_id='{result[2]}', movie_id='{result[3]}', start_time='{result[4]}' WHERE schedule_id='{result[0]}'")
            commitEdit = connection.execute(edit)
        return redirect(url_for('getSchedule'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule/remove', methods=['DELETE'])
def removeSchedule():
    try:
        form_data = request.form
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            remove = text(f"DELETE FROM schedule WHERE schedule_id={result[0]}")
            commitRemove = connection.execute(remove)
        return redirect(url_for('getSchedule'))
    except Exception as e:
        return jsonify(str(e))

if __name__ == '__main__':
    app.run()