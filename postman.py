from flask import Flask, jsonify, request, redirect, url_for, render_template
from sqlalchemy import create_engine
from sqlalchemy.sql import text
from datetime import datetime
import decimal

app = Flask(__name__)

conn_str = 'postgresql://postgres:Putranurelva2804?@localhost:5000/cinema_DB'
engine = create_engine(conn_str, echo=False)

@app.route('/')
def defaultUrl():
    return redirect(url_for('index'))

@app.route('/index', methods=['GET'])
def index():
    display = []
    try:
        with engine.connect() as connection:
            get_movie = text("SELECT title, description, directed_by, string_agg(cat_name, ', ') as categories FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            GROUP BY 1,2,3")
            movie_res = connection.execute(get_movie)
            for i in movie_res:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "category" : i[3]})
            return jsonify(display)

    except Exception as e:
        return jsonify(error=str(e))

@app.route('/index/diagram')
def diagram():
    return render_template('CinemaAppDiag.html')

@app.route('/index/<title>', methods=['GET'])
def details(title):
    try:
        with engine.connect() as connection:
            get_detail = text(f"SELECT movie_id, title, description, directed_by, release_year, duration_mins, string_agg(cat_name, ', ') as categories FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            WHERE title = '{title}'\
                            GROUP BY 1,2,3,4,5")
            detail_res = connection.execute(get_detail)
            for i in detail_res:
                title = i[1]
                desc = i[2]
                directed = i[3]
                release = i[4]
                duration = i[5]
                categories = i[6]
            return jsonify(title=title, description=desc, directed_by=directed, release_year=release, duration_mins=duration, category=categories)

    except Exception as e:
        return jsonify(error=str(e))

@app.route('/index/', methods=['GET'])
def search_movie():
    val = request.args.get("search")
    display = []
    try:
        with engine.connect() as connection:
            if val.isdigit():
                get_detail = text(f"SELECT title, description, directed_by, release_year, duration_mins, string_agg(cat_name, ', ')\
                            FROM movie JOIN movie_cat USING(movie_id) JOIN category USING (category_id)\
                            WHERE (title LIKE '%{val}%' OR description LIKE '%{val}%' OR directed_by LIKE '%{val}%' OR release_year = '{val}')\
                            GROUP BY 1,2,3,4,5")
            else:
                get_detail = text(f"SELECT title, description, directed_by, release_year, duration_mins, string_agg(cat_name, ', ')\
                            FROM movie JOIN movie_cat USING(movie_id) JOIN category USING (category_id)\
                            WHERE (title LIKE '%{val}%' OR description LIKE '%{val}%' OR directed_by LIKE '%{val}%')\
                            GROUP BY 1,2,3,4,5")

            detail_res = connection.execute(get_detail)
            for i in detail_res:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "release_year" : i[3], "duration_mins" : i[4], "category" : i[5]})
            return jsonify(display)

    except Exception as e:
        return jsonify(error=str(e))

@app.route('/index/movies/upcoming')
def indexUpcoming():
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT title, description, directed_by, release_year, duration_mins, string_agg(cat_name, ', ')\
                        FROM movie JOIN movie_cat USING(movie_id) JOIN category USING (category_id)\
                        WHERE movie_id NOT IN (Select movie_id from schedule group by 1)\
                        GROUP BY 1,2,3,4,5")
            movies = connection.execute(qry)
            for i in movies:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "release_year" : i[3], "duration_mins" : i[4], "category" : i[5]})
            return jsonify(display)

    except Exception as e:
        return jsonify(str(e))

@app.route('/index/movies/mostwatched')
def indexMost_watched():
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT title, description, directed_by, release_year, duration_mins, SUM(ticket_qty) as total FROM ticket JOIN schedule USING(schedule_id) JOIN movie USING(movie_id) GROUP BY 1,2,3,4,5 ORDER BY 6 DESC LIMIT 5")
            movies = connection.execute(qry)
            for i in movies:
                display.append({"title" : i[0], "description" : i[1], "directed_by" : i[2], "release_year" : i[3], "duration_mins" : i[4], "watched_by" : i[5]})
            return jsonify(display)

    except Exception as e:
        return jsonify(str(e))

@app.route('/signup/newuser', methods = ['POST'])
def regUser():
    form_data = request.json
    result = [value for key, value in form_data.items()]
    try :
        with engine.connect() as connection:
            try:
                check_email = text(f"SELECT email_address FROM public.user WHERE email_address = '{result[1]}'")
                run_verify = connection.execute(check_email)
                for h in run_verify:
                    verification = str(h[0])

                if verification == result[1]:
                    return jsonify(alert='That email address is already being used.\nPlease use another email address')

            except Exception:
                qry = text(f"INSERT INTO public.user (username, email_address, user_address) VALUES ('{result[0]}', '{result[1]}', '{result[2]}')")
                commit = connection.execute(qry)
                user = text(f"SELECT * FROM public.user WHERE username='{result[0]}' AND email_address='{result[1]}'")
                get_user = connection.execute(user)
                for i in get_user:
                    user_id = i[0]
                    username = i[1]
                    email_address = i[2]
                    user_address = i[3]
                    balance = i[4]

                return jsonify(user_id=user_id, username=username, email_address=email_address, user_address=user_address, balance=balance)
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/login/user', methods=['POST'])
def logged():
    user = []
    try:
        form_data = request.json
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            qry = text(f"SELECT * FROM public.user WHERE username = '{result[0]}' AND email_address = '{result[1]}'")
            get = connection.execute(qry)
            for i in get:
                user_id = i[0]
                username = i[1]
                email = i[2]
            if username == 'Admin' and email == 'admin@admin.com':
                return redirect(url_for('adminDash'))
            else:
                return redirect(url_for('userIndex', user_id=user_id))
    except Exception as e:
        print(str(e))
        return redirect(url_for('logIn'))

@app.route('/user/<user_id>/schedule', methods=['GET'])
def userIndex(user_id):
    play_times = []
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT user_id, username, email_address, balance FROM public.user WHERE user_id='{user_id}'")
            result = connection.execute(qry)
            for i in result:
                get_id = i[0]
                username = i[1]
                balance = int(i[3])
            schedule = text("SELECT play_date, day_id, day_name, movie_id, title, studio_id, price FROM public.schedule JOIN day_schedule using(day_id) JOIN movie using(movie_id) WHERE play_date = CURRENT_DATE GROUP BY 1,2,3,4,5,6,7 ORDER BY day_id, studio_id, movie_id")
            get_sched = connection.execute(schedule)
            for i in get_sched:
                display.append({"title":i[3], "play_date" : i[0], "day_name" : i[2], "title" : i[4], "studio" : i[5], "price" : int(i[6])})
            return jsonify(username=username, user_id=get_id, balance=balance, movies=display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/profile')
def userProfile(user_id):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT * FROM public.user WHERE user_id = {user_id}")
            user = connection.execute(qry)
            for i in user:
                display.append({"username": i[1], "email_address" : i[2], "user_address": i[3], "balance": int(i[4])})
        return jsonify(user_id=user_id, profile=display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/profile/edit', methods=['PUT'])
def editProfile(user_id):
    form_data = request.json
    result = [value for key, value in form_data.items() if value]
    print(result)
    try:
        with engine.connect() as connection:
            update = text(f"UPDATE public.user SET username='{result[0]}', user_address='{result[1]}' WHERE user_id={user_id}")
            commitUpdate = connection.execute(update)
            return redirect(url_for('userProfile', user_id=user_id))
    except Exception as e:
        return jsonify(str(e))
  
@app.route('/user/<user_id>/topup')
def topUp(user_id):
    return render_template('topUp.html', user_id=user_id)

@app.route('/user/<user_id>/topup/add', methods=['POST'])
def topUpAdd(user_id):
    form_data = request.json
    amount = [value for key, value in form_data.items()]
    try:
        with engine.connect() as connection:
            qry = text(f"UPDATE public.user SET balance='{int(amount[0])}' WHERE user_id='{user_id}'")
            commit = connection.execute(qry)
            updateTrx = text(f"INSERT INTO public.transaction (user_id, top_up) VALUES ({user_id},{int(amount[0])})")
            updateTrxCommit = connection.execute(updateTrx)
        return redirect(url_for('userIndex', user_id=user_id))

    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/buy', methods=['GET','POST'])
def moviePick(user_id):
    title = request.args.get('title')
    play_date = request.args.get('date')
    times = []
    try:
        with engine.connect() as connection:
            movie = text(f"SELECT movie_id, title, description, directed_by, release_year, duration_mins, string_agg(cat_name,', ') as category from movie join movie_cat using(movie_id) join category using(category_id) WHERE title='{title}' GROUP BY 1")
            get_movie = connection.execute(movie)
            for i in get_movie:
                movie_id = i[0]
                title = i[1]
                desc = i[2]
                directed = i[3]
                release = i[4]
                duration = i[5]
                cat = i[6] 
            schedule = text(f"SELECT start_time, play_date FROM schedule WHERE play_date='{play_date}' AND start_time >= CURRENT_TIME AND movie_id='{movie_id}'")
            get_sched = connection.execute(schedule)
            for j in get_sched:
                times.append(j[0].strftime("%H:%M"))
        return jsonify(title=title, play_date=play_date, description=desc, directed_by=directed, release_year=release, duration=duration, category=cat, schedule=times, user_id=user_id)

    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/buy/ticket', methods=['POST', 'GET'])
def confirmTix(user_id):
    title = request.args.get('title')
    play_date = request.args.get('date')
    time = request.args.get('time')
    try:
        with engine.connect() as connection:
            movie = text(f"SELECT movie_id, title, description, directed_by, release_year, duration_mins, string_agg(cat_name,', ') as category from movie join movie_cat using(movie_id) join category using(category_id) WHERE title='{title}' GROUP BY 1")
            get_movie = connection.execute(movie)
            for i in get_movie:
                movie_id = i[0]
                title = i[1]
                desc = i[2]
                directed = i[3]
                release = i[4]
                duration = i[5]
                cat = i[6] 

            priceQry = text(f"SELECT price FROM schedule JOIN day_schedule USING(day_id) WHERE play_date='{play_date}' AND start_time='{time}'")
            get_price = connection.execute(priceQry)
            for j in get_price:
                price = int(j[0])
        return jsonify(title=title, play_date=play_date, description=desc, directed_by=directed, release_year=release, duration=duration, category=cat, price=price, time=time, user_id=user_id)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/buy/ticket/confirm', methods=['POST', 'GET'])
def purchaseTix(user_id):
    form_data = request.json
    data = [value for key, value in form_data.items()]
    qty = int(data[0])
    title = request.args.get('title')
    play_date = request.args.get('date')
    time = request.args.get('time')
    try:
        with engine.connect() as connection:
            check_bal = text(f"SELECT balance FROM public.user WHERE user_id='{user_id}'")
            run_check_bal = connection.execute(check_bal)
            for i in run_check_bal:
                if i[0] == None:
                    return jsonify(user_id=user_id, alert='Jumlah saldo tidak mencukupi')
                else:
                    curBal = int(i[0])
            
            movieQry = text(f"SELECT movie_id FROM movie WHERE title='{title}'")
            get_movieid = connection.execute(movieQry)
            for j in get_movieid:
                movie_id = j[0]

            priceQry = text(f"SELECT schedule_id, price, capacity FROM schedule JOIN day_schedule USING(day_id) JOIN studio USING(studio_id) WHERE play_date='{play_date}' AND start_time='{time}' AND movie_id='{movie_id}'")
            get_price = connection.execute(priceQry)
            for k in get_price:
                schedule_id = k[0]
                price = int(k[1])
                capacity = k[2]

            check_seat = text(f"SELECT SUM(ticket_qty) FROM ticket WHERE schedule_id = {schedule_id}")
            run_check_seat = connection.execute(check_seat)
            for l in run_check_seat:
                seatTaken = j[0]

            totPrice = qty * price
            finalBal = curBal - totPrice

            if qty == 0 or qty == None:
                return jsonify(user_id=user_id, alert='Jumlah ticket harus diisi')
            elif totPrice > curBal:
                return jsonify(user_id=user_id, alert='Jumlah saldo tidak mencukupi')
            elif seatTaken > capacity:
                return jsonify(user_id=user_id, alert='Mohon maaf, tempat duduk sudah penuh')
            else:
                updateTix = text(f"INSERT INTO ticket (schedule_id, user_id, ticket_qty) VALUES ({schedule_id}, {user_id}, {qty})")
                updateTixCommit = connection.execute(updateTix)
                updateTrx = text(f"INSERT INTO public.transaction (user_id, purchase) VALUES ({user_id},{totPrice})") 
                updateTrxCommit = connection.execute(updateTrx)
                updateBal = text(f"UPDATE public.user SET balance={finalBal} WHERE user_id={user_id}")
                updateBalCommit = connection.execute(updateBal)
                return redirect(url_for('userIndex', user_id=user_id))

    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/trx/history')
def trxHistory(user_id):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT trx_id, top_up, purchase, trx_stamp FROM public.transaction WHERE user_id={user_id}")
            history = connection.execute(qry)
            for i in history:
                display.append({"trx_id" : i[0], "top_up" : str(i[1]), "purchase" : str(i[2]), "trx_stamp" : i[3]})
            return jsonify(user_id=user_id, history=display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/tix/history')
def tixHistory(user_id):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT ticket_id, schedule_id, ticket_qty FROM public.ticket WHERE user_id={user_id}")
            history = connection.execute(qry)
            for i in history:
                display.append({"ticket_id" : i[0], "schedule_id" : i[1], "ticket_qty" : i[2]})
            return jsonify(user_id=user_id, history=display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/user/<user_id>/tix/<ticket_id>')
def tixDetail(user_id, ticket_id):
    display = []
    try:
        with engine.connect() as connection:
            qry = text(f"SELECT ticket_id, title, play_date, description, directed_by, start_time, ticket_qty FROM public.ticket JOIN schedule USING(schedule_id) JOIN movie USING(movie_id) WHERE user_id={user_id} AND ticket_id={ticket_id}")
            detail = connection.execute(qry)
            for i in detail:
                display.append({"ticket_id" : i[0], "title" : i[1], "play_date" : i[2], "description" : i[3], "directed_by" : i[4], "start_time" : str(i[5]), "ticket_qty" : i[6]})
            return jsonify(user_id=user_id, detail=display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/report/user', methods=['GET'])
def allUser():
    display = []
    try :
        with engine.connect() as connection:
                qry = text("SELECT * FROM public.user ORDER BY 1")
                result = connection.execute(qry)
                for i in result:
                    display.append({"user_id": i[0], "username" : i[1], "email_address" : i[2], "user_address" : i[3], "balance" : str(i[4])})
                return jsonify(list=display)                
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/admin/report/tixpurchase', methods=['GET'])
def allPurchase():
    display = []
    try :
        with engine.connect() as connection:
                qry = text("SELECT ticket_id, play_date, start_time, movie_id, title, user_id, sum(ticket_qty*price) FROM ticket\
                            JOIN schedule using(schedule_id) JOIN movie using(movie_id) JOIN day_schedule using(day_id)\
                            GROUP BY 1,2,3,4,5,6\
                            ORDER BY 1")
                result = connection.execute(qry)
                for i in result:
                    display.append({"ticket_id" : i[0], "play_date" : i[1], "start_time" : str(i[2]), "movie_id" : i[3], "title" : i[4], "user_id" : i[5], "total" : int(i[6])})
                return jsonify(list=display)                
    except Exception as e:
        return jsonify(error=str(e))

@app.route('/admin/report/sales', methods=['GET'])
def allMovie():
    display = []
    try:
        with engine.connect() as connection:
            get_report = text("SELECT *, (SELECT SUM(ticket_qty) as ticket_sold FROM ticket as tix JOIN schedule as sched USING(schedule_id) JOIN day_schedule USING(day_id) JOIN studio USING(studio_id) JOIN movie USING(movie_id) WHERE sched.movie_id = mov.movie_id), (SELECT SUM(ticket_qty*price) as total_earning FROM ticket as tix JOIN schedule as sched USING(schedule_id) JOIN day_schedule USING(day_id) JOIN studio USING(studio_id) JOIN movie USING(movie_id) WHERE sched.movie_id = mov.movie_id) FROM movie as mov ORDER BY movie_id;")
            report = connection.execute(get_report)
            for i in report:
                display.append({"movie_id": i[0], "title" : i[1], "description" : i[2], "directed_by" : i[3], "release_year": i[4], "duration_mins" : i[5], "ticket_sold" : str(i[6]), "total_earning" : str(i[7])})
            return jsonify(list=display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies')
def getMovies():
    display = []
    try:
        with engine.connect() as connection:
            get_movies = text("SELECT movie.*, string_agg(cat_name, ', ') as categories FROM movie\
                            JOIN movie_cat USING(movie_id)\
                            JOIN category USING (category_id)\
                            GROUP BY 1,2,3,4,5,6 ORDER BY movie_id;")
            movies = connection.execute(get_movies)
            for i in movies:
                display.append({"movie_id": i[0], "title" : i[1], "description" : i[2], "directed_by" : i[3], "release_year": i[4], "duration_mins" : i[5], "categories" : i[6]})
            return jsonify(list=display)

    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies/add', methods=['POST'])
def addMovie():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items()]
        title = result[0].replace("'","''")
        description = result[1].replace("'","''")

        with engine.connect() as connection:
            add = text(f"INSERT INTO movie (title, description, directed_by, release_year, duration_mins) VALUES('{title}','{description}','{result[2]}',{int(result[3])},{int(result[4])})")
            commitAdd = connection.execute(add)

            getback = text(f"SELECT movie_id FROM movie WHERE title='{title}'")
            back = connection.execute(getback)
            movie_id = [i[0] for i in back]
            for i in result[5]:
                add_cat = text(f"INSERT INTO movie_cat (movie_id, category_id) VALUES ('{movie_id[0]}', '{i}')")
                commitCat = connection.execute(add_cat)

        return redirect(url_for('getMovies'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies/add/category', methods=['POST'])
def addCatMov():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items()]

        with engine.connect() as connection:
            for i in result[1]:
                addCat = text(f"INSERT INTO movie_cat (movie_id, category_id) VALUES ('{result[0]}', '{i}')")
                commitAddCat = connection.execute(addCat)

        return redirect(url_for('getMovies'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies/update', methods=['PUT'])
def editMovie():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items()]
        title = result[1].replace("'","''")
        description = result[2].replace("'","''")

        with engine.connect() as connection:
            edit = text(f"UPDATE movie SET title='{title}', description='{description}', directed_by='{result[3]}', release_year='{result[4]}', duration_mins='{result[5]}' WHERE movie_id={result[0]}")
            commitEdit = connection.execute(edit)

            getCurCat = text(f"SELECT category_id FROM movie_cat WHERE movie_id={result[0]}")
            getCat = connection.execute(getCurCat)
            curCat = [i[0] for i in getCat]

            for index,j in enumerate(curCat):
                updateCat = text(f"UPDATE movie_cat SET category_id='{result[6][index]}' WHERE movie_id={result[0]} AND category_id={j}")
                commitUpdateCat = connection.execute(updateCat)
        return redirect(url_for('getMovies'))

    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/movies/remove', methods=['DELETE'])
def removeMovie():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            removeCat = text(f"DELETE FROM movie_cat WHERE movie_id={result[0]}")
            commitRemoveCat = connection.execute(removeCat)
            remove = text(f"DELETE FROM movie WHERE movie_id={result[0]}")
            commitRemove = connection.execute(remove)
        return redirect(url_for('getMovies'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/categories')
def allCategories():
    display = []
    try:
        with engine.connect() as connection:
            get_cat = text("SELECT * FROM category")
            categories = connection.execute(get_cat)
            for i in categories:
                display.append({"category_id" : i[0], "cat_name" : i[1]})
            return jsonify(list=display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/categories/add', methods=['POST'])
def addCategories():
    data_form = request.json
    cat_name = data_form['cat_name']
    try:
        with engine.connect() as connection:
            add = text(f"INSERT INTO category (cat_name) VALUES ('{cat_name}')")
            commitAdd = connection.execute(add)
        return redirect(url_for('allCategories'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/categories/update', methods=['PUT'])
def updateCategories():
    data_form = request.json
    category_id = data_form['category_id']
    cat_name = data_form['cat_name']
    try:
        with engine.connect() as connection:
            update = text(f"UPDATE category SET cat_name='{cat_name}' WHERE category_id={category_id}")
            commitUpdate = connection.execute(update)
        return redirect(url_for('allCategories'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/categories/remove', methods=['DELETE'])
def removeCategories():
    data_form = request.json
    category_id = data_form['category_id']
    try:
        with engine.connect() as connection:
            remove = text(f"DELETE FROM category WHERE category_id={category_id}")
            commitRemove = connection.execute(remove)
        return redirect(url_for('allCategories'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule')
def getSchedule():
    try:
        with engine.connect() as connection:
            get_movies = text("SELECT movie_id, title FROM movie")
            legend = connection.execute(get_movies)
            legendDisp = [{"movie_id" : i[0], "title" : i[1]} for i in legend]

            studio1_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 1")
            studio1_get = connection.execute(studio1_movies)
            studio1 = [{"schedule_id": i[0], "play_date" : i[1], "day_id" : i[2], "day_name" : i[3], "movie_id" : i[4], "title" : i[5], "start_time": str(i[6])} for i in studio1_get]

            studio2_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 2")
            studio2_get = connection.execute(studio2_movies)
            studio2 = [{"schedule_id": i[0], "play_date" : i[1], "day_id" : i[2], "day_name" : i[3], "movie_id" : i[4], "title" : i[5], "start_time": str(i[6])} for i in studio2_get]

            studio3_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 3")
            studio3_get = connection.execute(studio3_movies)
            studio3 = [{"schedule_id": i[0], "play_date" : i[1], "day_id" : i[2], "day_name" : i[3], "movie_id" : i[4], "title" : i[5], "start_time": str(i[6])} for i in studio3_get]

            studio4_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 4")
            studio4_get = connection.execute(studio4_movies)
            studio4 = [{"schedule_id": i[0], "play_date" : i[1], "day_id" : i[2], "day_name" : i[3], "movie_id" : i[4], "title" : i[5], "start_time": str(i[6])} for i in studio4_get]

        return jsonify(legend=legendDisp, studio1=studio1, studio2=studio2, studio3=studio3, studio4=studio4)
    
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule/tab')
def getScheduleTab():
    try:
        with engine.connect() as connection:
            get_movies = text("SELECT movie_id, title FROM movie")
            legend = connection.execute(get_movies)

            studio1_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 1")
            studio1 = connection.execute(studio1_movies)

            studio2_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 2")
            studio2 = connection.execute(studio2_movies)

            studio3_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 3")
            studio3 = connection.execute(studio3_movies)

            studio4_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.schedule JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 4")
            studio4 = connection.execute(studio4_movies)

        return render_template('admin_editschedule.html', legend=legend, studio1=studio1, studio2=studio2, studio3=studio3, studio4=studio4)
    
    except Exception as e:
        return jsonify(str(e))


@app.route('/admin/edit/schedule/add', methods=['POST'])
def addSchedule():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items() if value]
        times = form_data['start_time']
        with engine.connect() as connection:
            for i in times:
                add = text(f"INSERT INTO schedule (day_id, play_date, movie_id, studio_id, start_time) VALUES ({result[1]},{result[2]},{result[3]},{result[0]},{i})")
                commitAdd = connection.execute(add)
        return redirect(url_for('getSchedule'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule/update', methods=['PUT'])
def editSchedule():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            edit = text(f"UPDATE schedule SET play_date='{result[1]}', day_id='{result[2]}', movie_id='{result[3]}', start_time='{result[4]}' WHERE schedule_id='{result[0]}'")
            commitEdit = connection.execute(edit)
        return redirect(url_for('getSchedule'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule/remove', methods=['DELETE'])
def removeSchedule():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items()]
        with engine.connect() as connection:
            remove = text(f"DELETE FROM schedule WHERE schedule_id={result[0]}")
            commitRemove = connection.execute(remove)
        return redirect(url_for('getSchedule'))
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule/mass/view', methods=['GET'])
def viewNewSchedule():
    display = []
    try:
        with engine.connect() as connection:
            view = text("SELECT day_id, play_date, movie_id, studio_id, start_time FROM playtable ORDER BY play_date, studio_id")
            create_view = connection.execute(view)
            for i in create_view:
                display.append({"day_id" : i[0], "play_date" : i[1], "movie_id" : i[2], "studio_id" : i[3], "start_time" : i[4].strftime("%H:%M")})

            return jsonify(list=display)
    except Exception as e:
        return jsonify(str(e))

@app.route('/admin/edit/schedule/mass/html', methods=['GET'])
def viewNewHtml():
    try:
        with engine.connect() as connection:
            get_movies = text("SELECT movie_id, title FROM movie")
            legend = connection.execute(get_movies)

            studio1_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.playtable JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 1")
            studio1 = connection.execute(studio1_movies)

            studio2_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.playtable JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 2")
            studio2 = connection.execute(studio2_movies)

            studio3_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.playtable JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 3")
            studio3 = connection.execute(studio3_movies)

            studio4_movies = text("SELECT schedule_id, play_date, day_id, day_name, movie_id, title, start_time FROM public.playtable JOIN day_schedule USING(day_id) JOIN movie USING(movie_id) WHERE studio_id = 4")
            studio4 = connection.execute(studio4_movies)

        return render_template('admin_editschedule.html', legend=legend, studio1=studio1, studio2=studio2, studio3=studio3, studio4=studio4)
    
    except Exception as e:
        return jsonify(str(e))


@app.route('/admin/edit/schedule/mass/new', methods=['POST'])
def createNewSchedule():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items() if value]
        times = form_data["start_time"]
        with engine.connect() as connection:
            for i in times:
                i = datetime.strptime(i, '%H:%M')
                create = text(f"INSERT INTO playtable (day_id, play_date, movie_id, studio_id, start_time)\
                            VALUES ({result[1]}, '{result[2]}', {result[3]}, {result[0]}, '{i}')")
                commitCreate = connection.execute(create)
            return redirect(url_for('viewNewSchedule'))

    except Exception as e:
        print(e)
        return jsonify(str(e))

@app.route('/admin/edit/schedule/mass/remove', methods=['DELETE'])
def tweakNewSchedule():
    try:
        form_data = request.json
        result = [value for key, value in form_data.items() if value]
        with engine.connect() as connection:
            remove = text(f"DELETE FROM playtable WHERE schedule_id = {result[0]}")
            commitRemove = connection.execute(remove)
        return redirect(url_for('viewNewSchedule'))

    except Exception as e:
        print(e)
        return jsonify(str(e))

@app.route('/admin/edit/schedule/mass', methods=['POST', 'DELETE'])
def massSchedule():
    try:
        term = request.args.get('term')
        afterdate = request.args.get('after')
        beforedate = request.args.get('before')
        with engine.connect() as connection:
            if term == 'daily':
                qry = text(f"INSERT INTO playtable (day_id, play_date, movie_id, studio_id, start_time)\
                            SELECT (pt.day_id + 1), (pt.play_date + 1), pt.movie_id, pt.studio_id, pt.start_time FROM playtable as pt\
                            WHERE pt.play_date >='{afterdate}'")
            elif term == 'weekly':
                qry = text(f"INSERT INTO playtable (day_id, play_date, movie_id, studio_id, start_time)\
                            SELECT pt.day_id, (pt.play_date + 7), pt.movie_id, pt.studio_id, pt.start_time FROM playtable as pt\
                            WHERE pt.play_date >='{afterdate}'")
            elif term == 'delete' and request.method == 'DELETE':
                qry = text(f"DELETE FROM playtable WHERE play_date > '{afterdate}' AND play_date < '{beforedate}'")
            else:
                return "Please input term type and/or date"

            commit = connection.execute(qry)
            return 'OK'
    except Exception as e:
        return jsonify(str(e))

if __name__ == '__main__':
    app.run()