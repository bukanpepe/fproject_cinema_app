CREATE DATABASE "cinema_DB"
    WITH 
    OWNER = postgres
    ENCODING = 'UTF8'
    CONNECTION LIMIT = -1;

CREATE TABLE public."user"
(
    user_id serial NOT NULL,
    username character varying(255) NOT NULL,
    email_address character varying(255) NOT NULL,
    user_address character varying(255),
    balance money,
    CONSTRAINT user_pkey PRIMARY KEY (user_id)
)

TABLESPACE pg_default;

CREATE TABLE public.category
(
	category_id serial NOT NULL,
    cat_name character varying(20),
    CONSTRAINT category_pkey PRIMARY KEY (category_id)
)

TABLESPACE pg_default;

CREATE TABLE public.movie
(
    movie_id serial NOT NULL,
    title character varying(255) NOT NULL,
    description text,
    directed_by character varying(255),
	release_year int,
	duration int,
    CONSTRAINT movie_pkey PRIMARY KEY (movie_id)
)

TABLESPACE pg_default;

CREATE TABLE public.movie_cat
(
    movie_id int NOT NULL,
	category_id int NOT NULL,
	CONSTRAINT fk_movie_id FOREIGN KEY (movie_id) REFERENCES movie (movie_id),
	CONSTRAINT fk_cat_id FOREIGN KEY (category_id) REFERENCES category (category_id)
)

TABLESPACE pg_default;

CREATE TABLE public.studio
(
    studio_id serial NOT NULL,
    capacity int NOT NULL,
    CONSTRAINT studio_pkey PRIMARY KEY (studio_id)
)

TABLESPACE pg_default;

CREATE TABLE public.day_schedule
(
    day_id serial NOT NULL,
    day_name character varying(20) NOT NULL,
	opening_hours time,
	closing_hours time,
    price numeric,
    CONSTRAINT day_pkey PRIMARY KEY (day_id)
)

TABLESPACE pg_default;

CREATE TABLE public.schedule
(
    schedule_id serial NOT NULL,
    day_id int NOT NULL,
    play_date date NOT NULL,
    movie_id int NOT NULL,
	studio_id int NOT NULL,
	start_time time NOT NULL,
	end_time time NOT NULL,
    CONSTRAINT schedule_pkey PRIMARY KEY (schedule_id),
    CONSTRAINT fk_day_id FOREIGN KEY (day_id) REFERENCES day_schedule (day_id),
	CONSTRAINT valid_date_day CHECK (extract(isodow from play_date) = day_id),
	CONSTRAINT fk_studio_id FOREIGN KEY (studio_id) REFERENCES studio (studio_id),
	CONSTRAINT fk_movie_id FOREIGN KEY (movie_id) REFERENCES movie (movie_id)
)

TABLESPACE pg_default;

CREATE TABLE public.ticket
(
    ticket_id serial NOT NULL,
    schedule_id int NOT NULL,
    user_id int NOT NULL,
    ticket_qty int NOT NULL,
    CONSTRAINT ticket_pkey PRIMARY KEY (ticket_id),
    CONSTRAINT fk_schedule_id FOREIGN KEY (schedule_id) REFERENCES schedule (schedule_id),
	CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.user (user_id)
)

TABLESPACE pg_default;

CREATE TABLE public.transaction
(
    trx_id serial NOT NULL,
    user_id int,
    top_up numeric,
    purchase numeric,
	trx_stamp timestamp NOT NULL,
    CONSTRAINT trx_pkey PRIMARY KEY (trx_id),
    CONSTRAINT fk_user_id FOREIGN KEY (user_id) REFERENCES public.user (user_id)
)

TABLESPACE pg_default;

CREATE FUNCTION valid_hours() RETURNS trigger AS $valid_hours$
    BEGIN
        IF NEW.day_id IS NULL THEN
            RAISE EXCEPTION 'day_id cannot be null';
        END IF;
        IF NEW.start_time IS NULL THEN
            RAISE EXCEPTION '% cannot have null salary', NEW.day_id;
        END IF;

        IF NEW.start_time > (Select closing_hours from day_schedule, schedule where NEW.day_id = day_schedule.closing_hours)
		THEN
            RAISE EXCEPTION '% cannot be after closing hours', NEW.day_id;
        END IF;
		RETURN NEW;
    END;
$valid_hours$ LANGUAGE plpgsql;

CREATE TRIGGER valid_hours BEFORE INSERT OR UPDATE ON schedule
    FOR EACH ROW EXECUTE PROCEDURE valid_hours();

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '1', '1', '12:00:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '1', '1', '14:45:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '1', '1', '17:30:00');
	
insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '1', '1', '20:15:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '5', '2', '12:15:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '5', '2', '14:45:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '5', '2', '17:15:00');
	
insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '5', '2', '19:45:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '4', '3', '12:30:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '4', '3', '15:00:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '4', '3', '17:30:00');
	
insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '4', '3', '20:00:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '8', '4', '12:45:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '8', '4', '15:15:00');

insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '8', '4', '17:45:00');
	
insert into public.schedule (
	day_id, play_date, movie_id, studio_id, start_time)
	VALUES ('7', '2020-12-20', '8', '4', '20:15:00');

